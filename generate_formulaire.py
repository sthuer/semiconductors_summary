#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Create a LaTeX formulaire from other .tex filex in directory.
The other files must have tags of the form %<*tag> and %</tag>, where tag is
also the label of the equation. The tag must be unique.

The script will create a file called Formelblatt.tex, which can be compiled
to a pdf.

Note: For the labels to work, the other .tex files need to be compiled first,
        so that the .aux files are created.

Note: It is assumed that the other .tex files use the hyperref package. If not,
        simply remove the hyperref package from the preamble of the generated
        file.

Attention: The script will overwrite Formelblatt.tex if it already exists.
"""


import os
import re
import subprocess


outfile = 'Formelblatt.tex'


COMPILE_IMMEDIATELY = True  # if False, you need to compile manually
PRECOMPILE_DEPENDENCIES = True  # if False, may cause incorrect references

AUTOMATICALLY_FIND_DEPENDENCIES = True  # if True, will compile all main .tex
#                                         files. If False, specify dependencies below
PRECOMPILE_FILES = ['semiconductor_summary.tex']  # specify dependencies here

shell_command = f"latexmk -pdf"

packages = r"""\documentclass[10pt, a4paper,multicol]{article}

%% Formelblatt, created by generate_formulaire.py
%% Inspired by QM formulaire on the  securepond https://drive.google.com/drive/folders/0B0mnTgFNEIOyLTd5RG9YdHJIYmM?resourcekey=0-87LA5TdV28mVRLz290wfWg&usp=sharing

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{fourier}
\usepackage{multicol}
\usepackage[english]{babel}

\usepackage{microtype}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{subcaption}
\usepackage{graphicx, xcolor}
\usepackage{pdflscape}
\usepackage{nopageno}
\usepackage{geometry}
\geometry{hmargin=0.21cm,vmargin=0.21cm} %0.2 , 0.2
\setlength{\parindent}{0cm}

\usepackage{float}


\usepackage{siunitx}
\usepackage{physics}

\usepackage{xr-hyper}
\usepackage{url}
\usepackage[pdfusetitle]{hyperref}
\hypersetup{
    pdfborder={0 0 0},
    colorlinks=true,
    linkcolor=black,
    citecolor=black,
    urlcolor=black,
    bookmarksopen=false,
}
\urlstyle{same}

\usepackage{catchfilebetweentags}
"""


new_commands = r"""

\newcommand{\squishlist}{
   \begin{list}{$\cdot$}
    { \setlength{\itemsep}{0pt}    \setlength{\parsep}{0pt}
      \setlength{\topsep}{0pt}     \setlength{\partopsep}{0pt}
      \setlength{\leftmargin}{1em} \setlength{\labelwidth}{1.1em}
      \setlength{\labelsep}{0.5em} }
}

\newcommand{\squishend}{
    \end{list}
}


\newcommand{\EqTag}[1]{\tag{\ref{#1}}}
\newcommand{\Eq}[2]{\begin{equation}
    \begin{split}
        #1
    \end{split}
    \EqTag{#2}
\end{equation}
}

"""

start_doc = r"""
\begin{document}
\begin{landscape}
    \begin{multicols}{4}
        \raggedcolumns
        \pagestyle{empty}
"""

end_doc = r"""
    \end{multicols}
\end{landscape}
\end{document}
"""


def get_files():
    t = os.listdir()
    t.sort()
    return (f for f in t if f.endswith('.tex') and f != outfile)


def get_tags(filename):
    with open(filename, 'r') as f:
        data = f.read()
    return re.findall(r'%<\*(.+)>', data)


def get_section(filename):
    with open(filename, 'r') as f:
        data = f.read()
    return re.findall(r'\\section{(.+)}', data)[0]


def is_main_tex(filename):
    with open(filename, 'r') as f:
        data = f.read()
    return bool(re.findall(r'\\begin{document}', data))


def main():
    print("==============================\n"
          "Generate Cheat Sheet\n"
          "==============================")
    ref_list = ""
    parse_list = ""
    eq_list = ""
    dep = []
    print("Writing preamble...")
    print("Parsing files...")
    for f in get_files():   
        print(4*' '+f)
        tags = get_tags(f)
        if is_main_tex(f):
            ref_list += f"\externaldocument{{{f[:-4]}}}\n"
            dep.append(f)

        if tags:
            print(8*' '+str(tags))
            eq_list += f"        {get_section(f)}\n"
            for t in tags:
                st = "\\"+t
                parse_list += f"        \\CatchFileBetweenTags{{{st}}}{{{f}}}{{{t}}}\n"
                eq_list += f"        \\Eq{{{st}}}{{{t}}}\n"

    print("Writing document...")
    with open(outfile, 'w') as o:
        o.write(packages)
        o.write("\n\n")
        o.write(new_commands)
        o.write("\n\n")
        o.write(ref_list)
        o.write(start_doc)
        o.write("\n        %%%%%%%%%%\n")
        o.write(parse_list)
        o.write("\n        %%%%%%%%%%\n")
        o.write(eq_list)
        o.write("\n        %%%%%%%%%%\n")
        o.write(end_doc)

    sh = shell_command

    compile_list = dep if AUTOMATICALLY_FIND_DEPENDENCIES else PRECOMPILE_FILES
    if PRECOMPILE_DEPENDENCIES and compile_list:
        sh = shell_command + f" {' '.join(compile_list)}"

    sh = sh + f" {outfile}"

    if os.path.exists(outfile) and COMPILE_IMMEDIATELY:
        print(f"Compiling {outfile}...")
        print("> "+sh)
        subprocess.call(sh, shell=True)

    print("Done.")


if __name__ == '__main__':
    main()
