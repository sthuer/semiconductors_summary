\section{Diode applications}
\subsection{Small signal}
If $i\ll I$ and $v\ll V$, then we can use the small signal approximation.
\begin{equation}
    \begin{split}
        I+i &= I_0\left(\exp\frac{q(V+v)}{kT}-1\right) \\
        &=I_0\left(\exp\frac{q(V)}{kT}\exp\frac{q(v)}{kT}-1\right)\\
        &\approx I_0\left(\exp\frac{q(V)}{kT}\left(1+\frac{qv}{kT}\right)-1\right)\\
        &= I_0\left( \exp\frac{qV}{kT} - 1 \right) + I_0 \left( \exp\left(\frac{qV}{kT}\right)\frac{qv}{kT} \right)
    \end{split}
\end{equation}

Which gives us the smallsignal current
\begin{align}
    i   & = \frac{q\left(I+I_0\right)}{kT}v = g_d v \\
    \label{EqGdSmallSignalDiode}
    %<*EqGdSmallSignalDiode>
    g_d & = \frac{q\left(I+I_0\right)}{kT}
    %</EqGdSmallSignalDiode>
\end{align}


\subsubsection{Capacitances of small-signal model}
\begin{circuit}[H]
    \centering
    \caption{Small signal model for diode}
    \begin{circuitikz}
        \draw (-3,2) to [Do] (-3,0);
        \draw[->] (-2,1) -- (-1,1);
        \draw (0,0) to [R,l=$g_d$] (0,2);
        \draw (2,0) to [C,l=$C_j$] (2,2);
        \draw (4,0) to [C,l=$C_d$] (4,2);
        \draw (0,0) to [short] (4,0);
        \draw (0,2) to [short] (4,2);
        \draw (2,2) to [short] (2,2.5);
        \draw (2,-0.5) to [short] (2,0);
    \end{circuitikz}
\end{circuit}
\begin{align}
    \label{EqCjCdSmallSignalDiode}
    %<*EqCjCdSmallSignalDiode>
    C_j    & = \frac{C_{j0}}{\sqrt{1-\frac{V}{\phi_B}}}        \\
    C_d    & = \frac{q}{kT}\tau_T I                            \\
    %</EqCjCdSmallSignalDiode>
    \tau_T & \equiv \text{equivalent transit time of carriers}
\end{align}
Where $C_j$ dominates in reverse bias and small forward bias,
and $C_d$ dominates in large forward bias ($V>\frac{\phi_B}{2}$).


\subsection{Large signal}
\subsubsection{Rectifier}
This is pretty much trivial.
\begin{circuit}[H]
    \centering
    \caption{Rectifier}
    \begin{circuitikz}
        \draw (0,-2) to [sV,l=$V_{in}$] ++(0,4)
        to [short] ++(2,0)
        to [Do,v=$V_D$] ++(0,-2)
        to [R,l=$R_L$,v=$V_o$] ++(2,0)
        to [Do] ++(0,-2)
        to [short] (0,-2);
        draw (4,0) to [Do] ++(0,2)
        to [short] ++(-2,0);
        \draw (2,-2) to [Do] ++(0,2);
        \draw (4,0) to [Do] ++(0,2)
        to [short] ++(-2,0);
    \end{circuitikz}
\end{circuit}
\begin{equation}
    V_o = V_{in}-2V_D
\end{equation}

\subsubsection{Voltage regulator}
\begin{circuit}[H]
    \centering
    \caption{Voltage regulator}
    \begin{circuitikz}
        \draw (0,0) to [V,v<=$10\pm1\ V$] (0,8)
        to [short] ++(2,0)
        to [R,l=$R_1$] ++(0,-2)
        to [short] ++(0,-2)
        to [Do] ++(0,-1)
        to [Do] ++(0,-1)
        to [Do] ++(0,-1)
        to [short] ++(0,-1)
        to [short] (0,0);
        \draw (2,5) to [nos] ++(2,0)
        to [R,l=$R_2$] ++(0,-5)
        to [short] (0,0);
    \end{circuitikz}
\end{circuit}

How to go about it:
\begin{align}
    I          & = \frac{V_{in}-\sum V_D}{R_1}                                            \\
    r_d        & =\left[\frac{\mathrm{d}I_D}{\mathrm{d}V_d}\right]^{-1} =\frac{nV_t}{I_0} \\
    r          & = \sum r_d                                                               \\
    \Delta V_o & = \Delta V_{in}\frac{r}{r+R_2}                                           \\[1em]
    n          & \equiv\text{non-ideality factor}                                         \\
    V_t        & = \frac{kT}{q}
\end{align}

Once the load is connected and draws current, we have a further small variation:
\begin{align}
    I_{load}   & = \frac{\sum V_D}{R_2} \\
    \Delta V_o & = I_{load}r
\end{align}

\subsection{Special diode types}
\subsubsection{Zener}
Is heavily doped making the depletion layer extremely thing, and thus allowing for QM tunneling in reverse biased diode.
(In this case known as band-to-band tunneling.)

\begin{figure}[H]
    \centering
    \caption[short]{Band-bending zener diode}
    \includegraphics[width=.75\textwidth]{imgs/zener_diode_band_bending.png}
\end{figure}
The voltage at which the diode starts conducting is called the zener voltage $V_Z$.
The diode then has a low resistance $R_Z$.

\subsubsection{Esaki}
Heavily doped with the tunneling effect in forward bias.
\begin{figure}[H]
    \centering
    caption{Esaki tunnel diode}
    \includegraphics[width=.75\textwidth]{imgs/esaki_tunnel_diode.png}
\end{figure}

\subsubsection{Schottky}
A schottky diode has $I_0$ $10^3$ to $10^8$ times bigger than a PN diode.
Preferred in low voltage high current applications.

\subsubsection{Photodiodes}
\begin{equation}
    I = I_0\left( e^{\frac{qV}{kT}} - 1\right)-I_{photo}
\end{equation}