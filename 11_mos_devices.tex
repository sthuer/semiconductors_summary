\section{MOS-FET}
\subsection{Capacitance}
\begin{circuit}[H]
    \centering
    \caption{MOS capacitance}
    \begin{circuitikz}
        \draw (0,0) node[above] (Vg){$V_{G}$}
        to [C,l=$C_{ox}$,o-*] ++(0,-1)  node[right] (vpsi){$\psi_s$}
        to [C,l=$C_{s}$] ++(0,-1)  node[ground](gnd){}
        ;
    \end{circuitikz}
\end{circuit}
\begin{align}
    \frac{1}{C_G} & = \frac{1}{C_{ox}}+\frac{1}{C_{s}}            \\
    C_s           & = \frac{\mathrm{d}\,(-Q_s)}{\mathrm{d}\,\psi_s}
\end{align}

\subsubsection{Parasitics and frequency response}
\begin{circuit}[H]
    \centering
    \caption{MOS parasitic capacitance}
    \label{circ:mos_capas_impure}
    \begin{circuitikz}
        \draw (0,0) node[above] (Vg){$V_{G}$}
        to [C,l=$C_{ox}$,o-] ++(0,-2)  node[above right] (vpsi){$\psi_s$};
        \draw (0,-2)--++(1,0) to [vC,l=$C_{Si}$,invert] ++(0,-2)  node[ground](gnd){};
        \draw (0,-2)--++(-1,0) to [vC,l=$C_{It}$,invert] ++(0,-2)  node[ground]{};
    \end{circuitikz}
\end{circuit}
\begin{align}
    C_{Si} & = \frac{\mathrm{d}\,(-Q_{Si})}{\mathrm{d}\,\psi_s}                                       \\
    C_{IT} & = \frac{\mathrm{d}\,\left|Q_{It}\right|}{\mathrm{d}\,\psi_s}\qquad\text{Interface traps}
\end{align}
\begin{figure}[H]
    \centering
    \caption{MOS Capacitance vs. frequency}
    \label{fig:mos_capas}
    \includegraphics[width=.6\textwidth]{imgs/MOS_cap_freq_response.png}
\end{figure}
\begin{equation}
    \frac{1}{C_{inv}}=\frac{W_{dm}}{\varepsilon_{Si}}+\frac{1}{C_{ox}}
\end{equation}

\subsection{MOS-FET operation}
Mostly trivial, but here's a summary:
\begin{equation}
    I_D  = -W Q_n(y) v_y(y)
\end{equation}
\subsubsection{Weak inversion}
In sub-threshold operation,
we find ourselves with diffusion current only.
This is usually referred to as `cutoff' region.
\begin{align}
    I_D & =q\frac{W}{L}\frac{n_i^2}{N_A}\frac{\left(k_B T /q\right)^2}{E_s}\mu e^{qV_{GS} / m k_B T}\left(1 - e^{-q V_{DS}/k_BT}\right) A \\
    m   & =1+\frac{C_d}{C_{ox}}                                                                                                    \\
    S   & = \frac{\mathrm{d}\,V_g}{\mathrm{d}\,\log I_D} \approx {\frac{kT}{q}} \ln{10} \approx \SI{60}{\milli\volt/decade}
\end{align}


\subsubsection{Deep triode}
Strong inversion, very small $V_{DS}$. (Strong inversion means $V_{GS}>V_T$)
(Note, this is essentially the same as triode, without the 2nd order term of $V_{DS}$)
\begin{align}
    I_D    & = W C_{ox}(V_{GS}-V_T)\mu_{eff}E_x                              \\
    E_x    & = \frac{V_{DS}}{L}                                              \\
    I_D    & = \mu_{eff}C_{ox}\frac{W}{L}(V_{GS}-V_T)V_{DS}                  \\
    R_{DS} & =\frac{V_{DS}}{I_{DS}} = \frac{L}{W\mu_{eff}C_{ox}(V_{GS}-V_T)}
\end{align}
\subsubsection{Triode}
Strong inversion, $V_{DS}<V_{GS}-V_T$. (Strong inversion means $V_{GS}>V_T$)
\begin{align}
    I_{DS} & = -\frac{W}{L}\mu\int_{V_S}^{V_D}C_{ox}Q_{inv}(V_C)\,dV_C               \\
    I_{DS} & = \mu C_{ox}\frac{W}{L} \left(V_{GS}-V_T-\frac{V_{DS}}{2} \right)V_{DS} \\
\end{align}

\subsubsection{Saturation}
Strong inversion ($V_{GS} > V_T$) and pinch off ($V_{DS}>V_{GS}-V_T$), this leads to $I_D$ being ``independent'' of $V_{DS}$. 
\begin{align}
    Q_i(x)                & =-C_{ox}(V_{GS}-V_T-V(x))                         \\
    V(L)                  & =V_{GS}-V_T                                       \\
    \Rightarrow Q_i(L)    & \approx 0                                         \\
    \Rightarrow I_{D,sat} & =\frac{1}{2}\frac{W}{L}\mu_n C_{ox}(V_{GS}-V_T)^2
\end{align}

\subsubsection{Channel length modulation}
If $V_{DS}$ is increased, the depletion region is pushed back by $\Delta L$.
\begin{align}
    I_D & = \frac{I_{D,sat}}{1-\frac{\Delta L}{L}}                                                                 \\
    I_D & = \frac{1}{2}\frac{W}{L}\mu_n C_{ox}(V_{GS}-V_T)^2\left[1+\lambda\left(V_{DS}-V_{DS_{sat}}\right)\right]
\end{align}
Note that, if this effect is neglifible, $\lambda\approx0$ we are in Long channel, else in Short channel.


\subsection{MOS-FET small-signal}
\begin{circuit}
    \caption[short]{MOS small-signal}
    \label{circ:nmos_small_signal}
    \centering
    \begin{circuitikz}
        \draw (0,0) node[left] (Vg){$G$}
        to [short,o-*] ++(1,0)
        to [open,v=$v_{gs}$] ++(0,-2)
        to [short,*-] ++(3,0)
        to [R,l=$r_o$] ++(0,2)
        ++(1,0) node[right] (D){$D$}
        to [short,o-] ++(-3,0)
        to [cI,i=$g_m v_{gs}$] ++(0,-2)
        to[short,-o]++(0,-1)node[below] (S){$S$}
        ;
        \draw (D.east)++(2,0) node(pmos){}
        to [short,*-o] ++(4,0) node[right](S){$S$}
        ++(-3,0) to [cI,i=$g_m v_{sg}$] ++(0,-2)
        -- ++(2,0)
        to [R,l=$r_o$] ++(0,2)
        ++(-3,0)
        to [open,v=$v_{sg}$] ++(0,-2)
        to [short,*-o] ++(-1,0) node[left](G){$G$}
        ++(3,0)
        to[short,-o]++(0,-1)node[below](D){$D$}
        ;
    \end{circuitikz}
\end{circuit}
\begin{align}
    \label{EqGmMOS}
    %<*EqGmMOS>
    g_m & = \mu C_{ox} \frac{W}{L} \left(V_{GS}-V_T\right) \\
        & = \frac{2I_{D}}{V_{GS}-V_T}                      \\
        & =\sqrt{2\mu_n C_{ox}\frac{W}{L}I_{D}}            \\
    r_o & = \frac{1}{\lambda I_{D}}
    %</EqGmMOS>
\end{align}
Note that for high frequency, add capacitances between each terminal,
and add a second current source $g_{mb}v_{bs}$.

\subsection{Scaling}
Constant electric field scaling:
\begin{table}[H]
    \caption{Dennard's rule}
    \label{tab:dennard}
    \centering
    \begin{tabular}{l r r}
                          & Before scaling & After scaling            \\
        \hline
        Channel length    & $L_{eff}$      & $\frac{L_{eff}}{\alpha}$ \\
        Oxide thickness   & $t_{ox}$       & $\frac{t_{ox}}{\alpha}$  \\
        Channel doping    & $N_B$          & $ N_B \cdot \alpha $     \\
        Operating Voltage & $V_{DD}$       & $\frac{V_{DD} }{\alpha}$ \\
    \end{tabular}
\end{table}



\subsection{Short channel effects}
\begin{minipage}[t]{.6\textwidth}
    $V_T$ roll-off:
    \begin{itemize}
        \item Lower $V_T$
        \item $I_{Off}$ increases
        \item Less control on channel
    \end{itemize}
\end{minipage}
\begin{minipage}{.4\textwidth}
    \begin{figure}[H]
        \centering
        \caption{Short channel effects}
        \label{fig:short_channel_effects}
        \includegraphics[width=\textwidth]{imgs/short_channel_rolloff.png}
    \end{figure}
\end{minipage}

\subsection{CMOS performance}
For `large' CMOS, we have the following.
Not applicable for nanometric CMOS.
\begin{equation}
    P = \frac{C V_{DD}^2}{T} = C V_{DD}^2 f
\end{equation}

\subsection{CMOS capacitances}
\begin{figure}[H]
    \centering
    \includegraphics[width=.8\textwidth]{imgs/MOSFET_caps.png}
    \caption{MOSFET capacitances in different regions.}\label{label:fig:mosfet_caps}
\end{figure}
