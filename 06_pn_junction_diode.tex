\section{PN junction diode}
\subsection{Carrier concentration under bias}
Under forward bias,  the net current is no longer zero.
\begin{equation}
    \left| J_{drift} \right|<\left| J_{diff} \right|
\end{equation}
Which causes injection of minority carriers into the QNR regions giving rise to `high' currents.


\subsection{Diode current}
To calculate the current, we \begin{enumerate}
    \item Calculate concentration of minority carriers at the edges of SCR
    \item Calculate minority carrier diffusion current in each QNR for $I_n$ and $I_p$
    \item Sum the currents $I_n$ and $I_p$
\end{enumerate}

\subsubsection{Minority carrier conditions}
We use the quasi-equilibrium equation to misuse equations for equilibrium.
\begin{align}
    \frac{n(x_1)}{n(x_2)} & \approx \exp{\frac{q(\phi(x_1)-\phi(x_2))}{kT}}  \\
    \frac{p(x_1)}{p(x_2)} & \approx \exp{\frac{-q(\phi(x_1)-\phi(x_2))}{kT}}
\end{align}

So by using $x_n$ and $x_p$ in the above equation we have the following:
\begin{align}
    \frac{n(x_n)}{n(-x_p)} & \approx \exp{\frac{q(\phi_B-V)}{kT}}  \\
    \frac{p(x_n)}{p(-x_p)} & \approx \exp{\frac{-q(\phi_B-V)}{kT}} \\
    p(-x_p)                & =N_a                                  \\
    n(x_n)                 & =N_d
\end{align}

And so we find what we needed:
\begin{align}
    n(-x_p) & \approx N_d\exp\frac{q(V-\phi_B)}{kT} \\
    p(x_n)  & \approx N_a\exp\frac{q(V-\phi_B)}{kT}
\end{align}


Then by using the Boltzman relations \eqref{EqBoltzmanPhiN} and \eqref{EqBoltzmanPhiP} we find
\begin{align}
    \phi_B              & = \frac{kT}{q}\ln\frac{N_dN_a}{n_i^2}      \\
    \Rightarrow n(-x_p) & \approx \frac{n_i^2}{N_a}\exp\frac{qV}{kT} \\
    \Rightarrow p(x_n)  & \approx \frac{n_i^2}{N_d}\exp\frac{qV}{kT}
\end{align}

\subsubsection{Diffusion current in QNR}
We assume a linear gradient between $n(-W_p)$ and $n(-x_p)$ to easily use \eqref{label:eq:diff_current_n} to find
\begin{equation}
    \begin{split}
        J_n^{diff} &= qD_n\frac{n_p(-x_p)-n_p(-W_p)}{W_p-x_p}\\
        &= qD_n \frac{\left(\frac{n_i^2}{N_a}\exp{\frac{qV}{kT}}\right)-\frac{n_i^2}{N_a}}{W_p-x_p}\\
        &= q\frac{n_i^2}{N_a}\frac{D_n}{W_p-x_p}\left(\exp{\frac{qV}{kT}}-1\right)
    \end{split}
\end{equation}

\subsubsection{Total diode current}
\begin{equation}
    \begin{split}
        \label{EqTotalDiodeCurrent}
        %<*EqTotalDiodeCurrent>
        J & = J_n+J_p                                                                                             \\
        & =q n_i^2 \left( \frac{1}{N_A}\frac{D_n}{W_p-x_p} + \frac{1}{N_D}\frac{D_p}{W_n-x_n} \right)\left(\exp
        \frac{qV}{kT} - 1\right)
        %</EqTotalDiodeCurrent>
    \end{split}
\end{equation}
Or so simplify
\begin{align}
    \label{EqTotalDiodeCurrentSimplified}
    %<*EqTotalDiodeCurrentSimplified>
    I   & =I_0\left(\exp\frac{qV}{kT}-1\right)                                 \\
    I_0 & = A q n_i^2 \left( \frac{D_n}{L_n N_A} + \frac{D_p}{L_p N_D} \right)
    %</EqTotalDiodeCurrentSimplified>
\end{align}

where we use the electron diffusion length $L_n$ and hole diffusion length $L_p$:
\begin{align}
    \label{EqDiffusionLengths}
    %<*EqDiffusionLengths>
    L_n & = \sqrt{D_n\tau_n}  = W_p-x_p \\
    L_p & = \sqrt{D_p\tau_p}  = W_n-x_n
    %</EqDiffusionLengths>
\end{align}


Note that sometimes a non-ideality factor $n$ is used:
\begin{equation}
    I  = I_0\left(\exp\frac{qV}{nkT}-1\right)
\end{equation}


\subsection{PN junction reverse bias}
When applying a reverse bias, the depletion region gets wider and the electric field increases.
There comes a point when the diode breaks down and destroys itself.
\begin{equation}
    W_{dep} = \sqrt{\frac{2\varepsilon}{q}\left(\frac{1}{N_A}+\frac{1}{N_D}\right)\left(V_0+V_R\right)}
\end{equation}